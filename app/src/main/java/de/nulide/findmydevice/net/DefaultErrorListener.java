package de.nulide.findmydevice.net;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import de.nulide.findmydevice.utils.Logger;

public class DefaultErrorListener implements Response.ErrorListener {

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        Logger.setDebuggingMode(true);
        Logger.log("VolleyError", volleyError.toString());
    }
}
